import 'react-datepicker/dist/react-datepicker.css';
import { RegisterEmp } from './features/registerEmp/RegisterEmp';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';

import configureStore from './configStore';

const { store, persistor } = configureStore();
function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <RegisterEmp />
      </PersistGate>
    </Provider>
  );
}

export default App;
