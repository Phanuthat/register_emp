import React from 'react';
import { MobileNumberStyleWapper } from './Style';
import Select from 'react-select';

const customStyles = {
  option: (provided, state) => ({ ...provided }),
  control: (base, state) => ({
    ...base,
    boxShadow: 'none',
    width: 120,
    minHeight: 30,
    height: 30,
    borderColor: 'black',
  }),
  valueContainer: (provided, state) => ({
    ...provided,
    height: '30px',
    padding: '0 6px',
  }),
  input: (provided, state) => ({
    ...provided,
    margin: '0px',
    paddingBottom: 5,
  }),
  indicatorSeparator: (state) => ({
    display: 'none',
  }),
  indicatorsContainer: (provided, state) => ({
    ...provided,
    height: '30px',
  }),
};
export function MobileNumber({
  title = '',
  isRequired = false,
  onChange = () => {},
  value = '',
  placeholder = [],
  options = [],
  onSelect = () => {},
  selectValue = {},
  error = '',
}) {
  return (
    <>
      <MobileNumberStyleWapper>
        <span>
          {title}:{isRequired && <span style={{ color: 'red' }}>*</span>}
        </span>
        <Select
          placeholder={<div>{placeholder}</div>}
          onChange={onSelect}
          defaultValue={options[0]}
          // value={selectValue}
          options={options}
          styles={customStyles}
          components={{
            IndicatorSeparator: () => null,
          }}
        />
        <div>
          <input className='input-wapper' onChange={onChange} value={value} />
          {!!error && isRequired && (
            <div style={{ display: 'flex', justifyContent: 'flex-start' }}>
              <span style={{ fontSize: 10, color: 'red' }}>{error}</span>
            </div>
          )}
        </div>
      </MobileNumberStyleWapper>
    </>
  );
}
