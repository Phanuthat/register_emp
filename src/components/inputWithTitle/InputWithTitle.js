import React from 'react';
import { InputStyleWapper } from './Style';

export function InputWithTitle(props) {
  return (
    <div>
      <InputStyleWapper>
        <span>
          {props.title}:
          {props.isRequired && <span style={{ color: 'red' }}>*</span>}
        </span>
        <input
          className='input-wapper'
          onChange={props.onChange}
          value={props.value}
        />
        {props.isShowText && <span>{props.text}</span>}
      </InputStyleWapper>
      {!!props.error && props.isRequired && (
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <span style={{ fontSize: 10, color: 'red' }}>{props.error}</span>
        </div>
      )}
    </div>
  );
}
InputWithTitle.defaultProps = {
  title: '',
  isRequired: false,
  onChange: () => {},
  value: '',
  isShowText: false,
  text: '',
  error: '',
};
