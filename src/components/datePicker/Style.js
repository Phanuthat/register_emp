import styled from 'styled-components';

export const DatePickerStyleWapper = styled.div`
  display: flex;
  width: 100%;
  gap: 16px;
  .select-container {
    height: 25px;
    width: 100%;
  }
`;
