import styled from 'styled-components';

export const RegisterEmpStyleWapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  padding-top: 16px;
  padding-bottom: 16px;
`;
