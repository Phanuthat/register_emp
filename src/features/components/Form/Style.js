import styled from 'styled-components';

export const FormStyleWapper = styled.div`
  padding: 16px;
  .container-wrap {
    display: flex;
    flex-direction: row;
    gap: 16px;
    flex-wrap: wrap;
  }
`;
