import { combineReducers } from 'redux';
import registerReducer from './features/registerEmp/reducers';
const allReducers = combineReducers({ register: registerReducer });

export default (state, action) => {
  return allReducers(state, action);
};
